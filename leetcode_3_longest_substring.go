package leetcode

import (
	"log"
	"math/big"
	"testing"
)

/*
* find longest substring in s, which do not have any repeating character in substring
* ie, 'wpew' would not be valid substring because 'w' repeats
 */
func lengthOfLongestSubstring(s string) int {
	var P []uint64 = make([]uint64, len(s)+1)
	var c rune
	var i int
	var b int = 0
	var b_max int = 0
	var l int = 1
	var l_max int = 1
	var n *big.Int = new(big.Int)
	var r *big.Int = new(big.Int)
	var q *big.Int = new(big.Int)

	if len(s) < 1 {
		return 0
	}

	for i, c = range s {
		P[i] = get_prime_number(c)
	}
	P[len(P)-1] = 1
	n.SetUint64(P[0])

	for i = 1; i < len(P); i++ {
		q.SetUint64(P[i])
		r.Rem(n, q)
		if r.String() == "0" {
			// Already contain same character
			if l > l_max {
				l_max = l
				b_max = b
			}
			b = i
			l = 1
			n.SetUint64(P[i])
		} else {
			n.Mul(n, q)
			l++
		}
	}

	if testing.Verbose() {
		log.Printf("Longest substring is %v\n", s[b_max:b_max+l_max])
	}
	return l_max
}

func get_prime_number(c rune) uint64 {
	switch c {
	case 'a':
		return 2
	case 'b':
		return 3
	case 'c':
		return 5
	case 'd':
		return 7
	case 'e':
		return 11
	case 'f':
		return 13
	case 'g':
		return 17
	case 'h':
		return 19
	case 'i':
		return 23
	case 'j':
		return 29
	case 'k':
		return 31
	case 'l':
		return 37
	case 'm':
		return 41
	case 'n':
		return 43
	case 'o':
		return 47
	case 'p':
		return 53
	case 'q':
		return 59
	case 'r':
		return 61
	case 's':
		return 67
	case 't':
		return 71
	case 'u':
		return 73
	case 'v':
		return 79
	case 'w':
		return 83
	case 'x':
		return 89
	case 'y':
		return 97
	case 'z':
		return 101
	default:
		return 1
	}
}
