package leetcode

import (
   "testing"
)

func test_leetcode_3_longest_substring(t *testing.T) {
   cases := []struct {
      in string
      want int
      got int
   }{
      {
         "",
         0,
         0,
      },
      {
         "a",
         1,
         0,
      },
      {
         "aa",
         1,
         0,
      },
      {
         "ab",
         2,
         0,
      },
      {
         "abccbb",
         3,
         0,
      },
      {
         "aabbcd",
         3,
         0,
      },
      {
         "abcabcbb",
         3,
         0,
      },
      {
         "bbbbb",
         1,
         0,
      },
      {
         "pwwkew",
         3,
         0,
      },
      {
         "dvdf",
         3,
         0,
      },
   }

   for i := 0; i < len(cases); i++ {
      cases[i].got = lengthOfLongestSubstring(cases[i].in)
      if cases[i].got != cases[i].want {
         t.Errorf("case %v, in: %v, want: %v, got: %v", i, cases[i].in, cases[i].want, cases[i].got)
      }
   }
}
