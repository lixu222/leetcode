package leetcode

import (
	"log"
	"testing"
)

type Median struct {
	idx int
	val float64
}

type slice_state int

const (
	// slice 1 last element less than or equal slice 2 first element
	S1_less_S2 slice_state = iota
	// slice 2 last element less than or equal slice 1 first element
	S2_less_S1
	// slice 1 first element less than or equal slice 2 first element
	// slice 1 last element greater than or equal slice 2 first element
	// slice 1 is guaranteed to have 2 elements
	S1_mix_S2
	// slice 2 first element less than or equal slice 1 first element
	// slice 2 last element greater than or equal slice 1 first element
	// slice 2 is guaranteed to have 2 elements
	S2_mix_S1
)

// input slice must not be empty
func get_slice_state(slice1 []int, slice2 []int) slice_state {
	var state slice_state
	if slice1[len(slice1)-1] <= slice2[0] {
		state = S1_less_S2
	} else if slice2[len(slice2)-1] <= slice1[0] {
		state = S2_less_S1
	} else if slice1[0] <= slice2[0] {
		state = S1_mix_S2
	} else {
		state = S2_mix_S1
	}
	return state
}

// input slice must not be empty
// input slice should be in state: S1_mix_S2
// with S1 being smaller and S2 being bigger
func get_splice_pt(smaller []int, bigger []int) []int {
	var tmp []int
	var state slice_state = S1_mix_S2

	if testing.Verbose() {
		log.Printf("get_splice_pt: smaller = %v\n", smaller)
		log.Printf("get_splice_pt: bigger = %v\n", bigger)
	}

	tmp = smaller
	for state != S1_less_S2 {
		tmp = tmp[:len(tmp)/2]
		if testing.Verbose() {
			log.Printf("get_splice_pt: tmp = %v\n", tmp)
		}
		state = get_slice_state(tmp, bigger)
		if testing.Verbose() {
			log.Printf("get_splice_pt: state = %v\n", state)
		}
	}
	return tmp
}

/*
* Given 2 sorted integer arrays (sorted from smallest to largest),
* find median value of the combined array from both arrays.
* Algorithm needs to be completed in log time
*
* Hint: binary search
 */
func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {
	var med *Median
	var nums, slice1, slice2, tmp []int
	var state slice_state

	slice1 = nums1
	slice2 = nums2
	if testing.Verbose() {
		log.Printf("findMedianSortedArrays: slice1 = %v\n", slice1)
		log.Printf("findMedianSortedArrays: slice2 = %v\n", slice2)
	}
	for len(slice1) > 0 || len(slice2) > 0 {
		if len(slice1) == 0 {
			nums = append(nums, slice2...)
			slice2 = nil
		} else if len(slice2) == 0 {
			nums = append(nums, slice1...)
			slice1 = nil
		} else {
			// Neither slice is empty
			state = get_slice_state(slice1, slice2)
			if testing.Verbose() {
				log.Printf("findMedianSortedArrays(): state = %v\n", state)
			}

			if state == S1_less_S2 {
				nums = append(nums, slice1...)
				nums = append(nums, slice2...)
				slice1 = nil
				slice2 = nil
			} else if state == S2_less_S1 {
				nums = append(nums, slice2...)
				nums = append(nums, slice1...)
				slice1 = nil
				slice2 = nil
			} else if state == S1_mix_S2 {
				tmp = get_splice_pt(slice1, slice2)
				nums = append(nums, tmp...)
				// slice1 must have one more element than tmp
				// should be guaranteed by algorithm
				slice1 = slice1[len(tmp):]
			} else {
				// state == S2_mix_S1
				tmp = get_splice_pt(slice2, slice1)
				nums = append(nums, tmp...)
				slice2 = slice2[len(tmp):]
			}
		}
	}

	if testing.Verbose() {
		log.Printf("sorted arrays: %v\n", nums)
	}
	med = get_median(nums)
	return med.val
}

// return sorted arrays' median value
func get_median(nums []int) *Median {
	var med *Median = new(Median)

	if len(nums) < 1 {
		// Empty array
		med.idx = -1
		return med
	}
	med.idx = len(nums) / 2
	if (len(nums) % 2) == 1 {
		// odd number
		med.val = (float64)(nums[med.idx])
	} else {
		// even number
		med.val = (float64)(nums[med.idx-1]+nums[med.idx]) / 2
	}
	return med
}
