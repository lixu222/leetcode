package leetcode

import "testing"

func test_leetcode_5_palindrom(t *testing.T) {
	var i int
	var cases []struct {
		b    string
		want string
		got  string
	}

	cases = []struct {
		b    string
		want string
		got  string
	}{
		{
			"a",
			"a",
			"",
		},
		{
			"aba",
			"aba",
			"",
		},
		{
			"aa",
			"aa",
			"",
		},
		{
			"abba",
			"abba",
			"",
		},
		{
			"ab",
			"a",
			"",
		},
		{
			"eabcb",
			"bcb",
			"",
		},
		{
			"adam",
			"ada",
			"",
		},
	}

	for i = 0; i < len(cases); i++ {
		cases[i].got = longestPalindrome(cases[i].b)
		if cases[i].got != cases[i].want {
			t.Errorf("case %v: want = %v, got = %v", i, cases[i].want, cases[i].got)
		}
	}
}
