package leetcode

import (
   "bytes"
   "strconv"
)

// singly-linked list
type ListNode struct{
   Val int
   Next *ListNode
}

func (l *ListNode) String() string {
   var buf bytes.Buffer
   if l == nil {
      return ""
   } else {
      for l != nil {
         buf.WriteString(strconv.Itoa(l.Val))
         l = l.Next
      }
      return buf.String()
   }
}

/*
* append node to end of linked list with specific val
*/
func append_node(l *ListNode, val int) {
   if l != nil {
      var p *ListNode = l
      for p.Next != nil {
         p = p.Next
      }
      p.Next = &ListNode{
         val,
         nil,
      }
   }
}

/*
* two numbers are represented using linked list, with each list item representing a digit and lsb first in list
* return their sum in linked list format
*/
func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
   var l3 *ListNode 
   var carry int = 0
   var val1 int
   var val2 int
   var sum int
   var dig int
   var p3 *ListNode
   var tmp *ListNode

   for (l1 != nil) || (l2 != nil) || (carry != 0){
      if l1 != nil {
         val1 = l1.Val
      } else {
         val1 = 0
      }
      if l2 != nil {
         val2 = l2.Val
      } else {
         val2 = 0
      }

      sum = val1 + val2 + carry
      dig = sum % 10
      tmp = &ListNode {
         dig,
         nil,
      }

      if l3 == nil {
         l3 = tmp
         p3 = l3
      } else {
         p3.Next = tmp
         p3 = p3.Next
      }

      carry = sum / 10
      if l1 != nil {
         l1 = l1.Next
      }
      if l2 != nil {
         l2 = l2.Next
      }
   }
   return l3
}
