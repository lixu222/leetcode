// given array of integers, return the two indices which sum to specific value
//
// assume there is only one set of indices which produce specific value
//
// because the solution exist and is unique, we search through all combinations to find a combination which satisfy specific value
package leetcode

func twoSum(nums []int, target int) []int {
   for i := 0; i < len(nums); i++ {
      for j := i + 1; j < len(nums); j++ {
         if (nums[i] + nums[j]) == target {
            return []int{i, j}
         }
      }
   }
   return nil
}


