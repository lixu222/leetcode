package leetcode

func min(a, b int) int {
	if a < b {
		return a
	} else {
		return b
	}
}

func longestPalindrome(s string) string {
	var T []byte
	var P []int
	var i, i_r int
	var L, C, R, L_s, R_s int
	var i_max int

	if len(s) <= 1 {
		return s
	}
	T = make([]byte, 2*len(s)+1)
	P = make([]int, len(T))
	for i = 0; i < len(T); i++ {
		if i%2 == 0 {
			// even
			T[i] = '#'
		} else {
			// odd
			T[i] = s[(i-1)/2]
		}
	}
	P[0] = 0
	P[1] = 1
	P[len(P)-1] = 0
	C = P[1]
	R = P[2]

	for i = 0; i <= len(T)-2; i++ {
		if i > R {
			P[i] = 0
		} else {
			i_r = C - (i - C)
			P[i] = min(P[i_r], R-i)
		}
		L_s = i - (P[i] + 1)
		R_s = i + P[i] + 1
		for L_s >= 0 && R_s < len(P) {
			if T[L_s] != T[R_s] {
				break
			}
			P[i]++
			C = i
			L_s = i - (P[i] + 1)
			R_s = i + P[i] + 1
			if P[i] > P[i_max] {
				i_max = i
			}
		}
	}

	L = (i_max - P[i_max]) / 2
	return s[L : L+P[i_max]]
}
