package leetcode

import (
	"testing"
)

func test_leetcode_4_median_array(t *testing.T) {
	cases := []struct {
		nums1 []int
		nums2 []int
		want  float64
		got   float64
	}{
		{
			[]int{},
			[]int{},
			0,
			0,
		},
		{
			[]int{1, 2, 3},
			[]int{},
			2,
			0,
		},
		{
			[]int{},
			[]int{4, 5, 6, 7},
			5.5,
			0,
		},
		{
			[]int{1},
			[]int{2},
			1.5,
			0,
		},
		{
			[]int{2},
			[]int{1, 3, 4, 5},
			3,
			0,
		},
	}

	for i := 0; i < len(cases); i++ {
		cases[i].got = findMedianSortedArrays(cases[i].nums1, cases[i].nums2)
		if cases[i].got != cases[i].want {
			t.Errorf("case %v: want = %v, got = %v", i, cases[i].want, cases[i].got)
		}
	}
}
