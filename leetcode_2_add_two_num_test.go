package leetcode

import (
   "testing"
)

func test_leetcode_2_add_two_num(t *testing.T) {
   var l1 = &ListNode{
      0,
      nil,
   }
   append_node(l1, 8)
   append_node(l1, 9)
   t.Logf("l1 = %v", l1)
   var l2 =&ListNode{
      0,
      nil,
   }
   append_node(l2, 2)
   t.Logf("l2 = %v", l2)
   var l3 = addTwoNumbers(l1, l2)
   t.Logf("l3 = %v", l3)

   var correct_sum = "0001"
   if l3.String() != correct_sum {
      t.Errorf("l3 is %v, but correct sum is %v", l3, correct_sum)
   }
}
