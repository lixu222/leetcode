package leetcode

import (
   "testing"
   "reflect"
)


func test_leetcode_1_two_sum(t *testing.T) {
   case1 := []int { 1, 2, 2, 4 }
   target1 := 4
   correct_result1 := []int {1, 2}
   actual_result1 := twoSum(case1, target1)
   if !reflect.DeepEqual(correct_result1, actual_result1) {
      t.Errorf("twoSum error for case1")
      t.Logf("correct_result1 = %v", correct_result1)
      t.Logf("actual_result1 = %v", actual_result1)
   }
}
